# Assignment1: RPG Heroes

## How to run
This application contains the necessary classes to set up the game, but cannot be played interactively.
Instead, please run all the JUnit tests to make sure everything is working as intended.