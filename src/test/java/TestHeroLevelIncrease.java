import org.example.*;
import org.junit.Test;

// TEST SET 2: Increase a hero's level by 1, make sure the level is increased by the right amount,
// and results in the correct attributes.
public class TestHeroLevelIncrease {
    @Test
    public void levelUpMage_byOne_correctLvlAndAttr() {
        // ARRANGE & ACT
        String name = "Gandalf";

        Hero testMage = new Mage(name);
        testMage.levelUp();

        // ASSERT
        int expectedNewLevel = 2;
        int expNewStrength = 1 + 1, expNewDexterity = 1 + 1, expNewIntelligence = 8 + 5;

        assert testMage.getLevel() == expectedNewLevel;
        assert (testMage.totalAttributes().getStrength() == expNewStrength);
        assert (testMage.totalAttributes().getDexterity() == expNewDexterity);
        assert (testMage.totalAttributes().getIntelligence() == expNewIntelligence);
    }

    @Test
    public void levelUpRanger_byOne_correctLvlAndAttr() {
        // ARRANGE & ACT
        String name = "Rango";

        Hero testRanger = new Ranger(name);
        testRanger.levelUp();

        // ASSERT
        int expectedNewLevel = 2;
        int expNewStrength = 1 + 1, expNewDexterity = 7 + 5, expNewIntelligence = 1 + 1;

        assert testRanger.getLevel() == expectedNewLevel;
        assert (testRanger.totalAttributes().getStrength() == expNewStrength);
        assert (testRanger.totalAttributes().getDexterity() == expNewDexterity);
        assert (testRanger.totalAttributes().getIntelligence() == expNewIntelligence);
    }

    @Test
    public void levelUpRouge_byOne_correctLvlAndAttr() {
        // ARRANGE & ACT
        String name = "Rick Deckard";

        Hero testRouge = new Rouge(name);
        testRouge.levelUp();

        // ASSERT
        int expectedNewLevel = 2;
        int expNewStrength = 2 + 1, expNewDexterity = 6 + 4, expNewIntelligence = 1 + 1;

        assert testRouge.getLevel() == expectedNewLevel;
        assert (testRouge.totalAttributes().getStrength() == expNewStrength);
        assert (testRouge.totalAttributes().getDexterity() == expNewDexterity);
        assert (testRouge.totalAttributes().getIntelligence() == expNewIntelligence);
    }

    @Test
    public void levelUpWarrior_byOne_correctLvlAndAttr() {
        // ARRANGE & ACT
        String name = "Pr. Mononoke";

        Hero testWarrior = new Warrior(name);
        testWarrior.levelUp();

        // ASSERT
        int expectedNewLevel = 2;
        int expNewStrength = 5 + 3, expNewDexterity = 2 + 2, expNewIntelligence = 1 + 1;

        assert testWarrior.getLevel() == expectedNewLevel;
        assert (testWarrior.totalAttributes().getStrength() == expNewStrength);
        assert (testWarrior.totalAttributes().getDexterity() == expNewDexterity);
        assert (testWarrior.totalAttributes().getIntelligence() == expNewIntelligence);
    }
}
