import org.example.Armor;
import org.example.Hero;
import org.example.HeroAttribute;
import org.example.Mage;
import org.junit.Test;

// TEST SET 5: equip Hero with Armor, check that totalAttributes are calculated correctly.
public class TestHeroWithArmorAttributes {
    @Test
    public void checkTotalAttributes_noArmor_correctTotalAttributes() throws Exception { // TODO: handle exception?
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // ASSERT
        // Check that the Hero's totalAttributes are at the value expected when with no armor
        HeroAttribute totalAttributes = testMage.totalAttributes();
        int expStrength = 1, expDexterity = 1, expIntelligence = 8;
        assert totalAttributes.getStrength() == expStrength;
        assert totalAttributes.getDexterity() == expDexterity;
        assert totalAttributes.getIntelligence() == expIntelligence;
    }

    @Test
    public void checkTotalAttributes_onePieceOfArmor_correctTotalAttributes() throws Exception { // TODO: handle exception?
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create Armor, with valid parameters
        String armorName = "Colorful Second-Hand Cape";
        int requiredLevel = 1;
        Hero.Slot slot = Hero.Slot.BODY;
        Armor.ArmorType armorType = Armor.ArmorType.CLOTH;
        int armorStrength = 0, armorDexterity = 1, armorIntelligence = 2; // Set up armorAttributes
        HeroAttribute armorAttribute = new HeroAttribute(armorStrength, armorDexterity, armorIntelligence);

        Armor testArmor = new Armor(armorName, requiredLevel, slot, armorType, armorAttribute);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Equip armor
        testMage.equip(testArmor);

        // ASSERT
        // Check that the Hero's totalAttributes are at the value expected when wearing the cape
        // Yeah, this is the same as one of the tests for equipping armor, since checking the resulting totalAttributes
        // is how I make sure it's been equipped properly.
        HeroAttribute totalAttributes = testMage.totalAttributes();
        int expStrength = 1, expDexterity = 1 + 1, expIntelligence = 8 + 2;
        assert totalAttributes.getStrength() == expStrength;
        assert totalAttributes.getDexterity() == expDexterity;
        assert totalAttributes.getIntelligence() == expIntelligence;
    }

        @Test
        public void checkTotalAttributes_twoPiecesOfArmor_correctTotalAttributes() throws Exception { // TODO: handle exception?
            // ARRANGE & ACT
            // Create Hero
            String heroName = "Gandalf";
            Hero testMage = new Mage(heroName);

            // Create first piece of Armor, with valid parameters
            String nameArmorOne = "Colorful Second-Hand Cape";
            int requiredLevelOne = 1;
            Hero.Slot slotOne = Hero.Slot.BODY;
            Armor.ArmorType armorTypeOne = Armor.ArmorType.CLOTH;
            int armorStrengthOne = 0, armorDexterityOne = 1, armorIntelligenceOne = 2; // Set up armorAttributes
            HeroAttribute armorAttributeOne = new HeroAttribute(armorStrengthOne, armorDexterityOne, armorIntelligenceOne);

            Armor testArmorOne = new Armor(nameArmorOne, requiredLevelOne, slotOne, armorTypeOne, armorAttributeOne);
            // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

            // Equip armor
            testMage.equip(testArmorOne);

            // Create SECOND piece of Armor, with valid parameters
            String armorNameTwo = "Very Drab Scarf";
            int requiredLevelTwo = 1;
            Hero.Slot slotTwo = Hero.Slot.HEAD;
            Armor.ArmorType armorTypeTwo = Armor.ArmorType.CLOTH;
            int armorStrengthTwo = 1, armorDexterityTwo = 0, armorIntelligenceTwo = 5; // Set up armorAttributes
            HeroAttribute armorAttributeTwo = new HeroAttribute(armorStrengthTwo, armorDexterityTwo, armorIntelligenceTwo);

            Armor testArmor = new Armor(armorNameTwo, requiredLevelTwo, slotTwo, armorTypeTwo, armorAttributeTwo);
            // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

            // Equip armor
            testMage.equip(testArmor);

            // ASSERT
            // Check that the Hero's totalAttributes are at the value expected when wearing the cape and the scarf
            HeroAttribute totalAttributes = testMage.totalAttributes();
            int expStrength = 1 + 1, expDexterity = 1 + 1, expIntelligence = 8 + 2 + 5;
            assert totalAttributes.getStrength() == expStrength;
            assert totalAttributes.getDexterity() == expDexterity;
            assert totalAttributes.getIntelligence() == expIntelligence;
        }

    @Test
    public void checkTotalAttributes_twoPiecesOfArmorOneReplaced_correctTotalAttributes() throws Exception { // TODO: handle exception?
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create first piece of Armor, with valid parameters
        String nameArmorOne = "Colorful Second-Hand Cape";
        int requiredLevelOne = 1;
        Hero.Slot slotOne = Hero.Slot.BODY;
        Armor.ArmorType armorTypeOne = Armor.ArmorType.CLOTH;
        int armorStrengthOne = 0, armorDexterityOne = 1, armorIntelligenceOne = 2; // Set up armorAttributes
        HeroAttribute armorAttributeOne = new HeroAttribute(armorStrengthOne, armorDexterityOne, armorIntelligenceOne);

        Armor testArmorOne = new Armor(nameArmorOne, requiredLevelOne, slotOne, armorTypeOne, armorAttributeOne);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Equip armor
        testMage.equip(testArmorOne);

        // Create SECOND piece of Armor, with valid parameters
        String armorNameTwo = "Very Drab Scarf";
        int requiredLevelTwo = 1;
        Hero.Slot slotTwo = Hero.Slot.HEAD;
        Armor.ArmorType armorTypeTwo = Armor.ArmorType.CLOTH;
        int armorStrengthTwo = 1, armorDexterityTwo = 0, armorIntelligenceTwo = 5; // Set up armorAttributes
        HeroAttribute armorAttributeTwo = new HeroAttribute(armorStrengthTwo, armorDexterityTwo, armorIntelligenceTwo);

        Armor testArmorTwo = new Armor(armorNameTwo, requiredLevelTwo, slotTwo, armorTypeTwo, armorAttributeTwo);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Equip armor
        testMage.equip(testArmorTwo);

        // REPLACE the second piece of Armor (still valid parameters)
        String armorNameThree = "Fancy Flower-Pattern Scarf";
        int requiredLevelThree = 1;
        Hero.Slot slotThree = Hero.Slot.HEAD;
        Armor.ArmorType armorTypeThree = Armor.ArmorType.CLOTH;
        int armorStrengthThree = 2, armorDexterityThree = 2, armorIntelligenceThree = 6; // Set up armorAttributes
        HeroAttribute armorAttributeThree = new HeroAttribute(armorStrengthThree, armorDexterityThree, armorIntelligenceThree);

        Armor testArmorThree = new Armor(armorNameThree, requiredLevelThree, slotThree, armorTypeThree, armorAttributeThree);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Equip armor
        testMage.equip(testArmorThree);

        // ASSERT
        // Check that the Hero's totalAttributes are at the value expected when wearing the cape and the newly replaced scarf
        HeroAttribute totalAttributes = testMage.totalAttributes();
        int expStrength = 1 + 2, expDexterity = 1 + 1 + 2, expIntelligence = 8 + 2 + 6;
        assert totalAttributes.getStrength() == expStrength;
        assert totalAttributes.getDexterity() == expDexterity;
        assert totalAttributes.getIntelligence() == expIntelligence;
    }
}
