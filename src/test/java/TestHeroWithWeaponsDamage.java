import org.example.*;
import org.junit.Test;

// TEST SET 6: equip Hero with Weapons, check that damage is calculated correctly.
public class TestHeroWithWeaponsDamage {
    @Test
    public void checkDamage_noWeapons_correctDamage() throws Exception { // TODO: handle exception?
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // ASSERT
        // Check that the Hero's damage level is at the value expected when urarmed
        int weaponDamage = 1; // Defaults to 1 when unarmed
        float expHeroDamage = weaponDamage * (1f + (8f/100f)); // 8 == intelligence, the Mage's 'damagingAttribute'
        assert testMage.damage() == expHeroDamage;
    }

    @Test
    public void checkDamage_weaponEquipped_correctDamage() throws Exception { // TODO: handle exception?
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create Weapon, with valid parameters
        String weaponName = "Bargain Basement Wand";
        int requiredLevel = 1;
        Weapon.WeaponType weaponType = Weapon.WeaponType.WAND;
        int weaponDamage = 2;

        Weapon testWeapon = new Weapon(weaponName, requiredLevel, weaponType, weaponDamage);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Equip weapon
        testMage.equip(testWeapon);

        // ASSERT
        // The level 1 Mage's capacity for damage when armed with a 'Bargain Basement Wand' (damage: 2) should be...
        // Yes, just like one of the tests in TestHeroItemEquip, because if the damage is right, the weapon was equipped successfully.
        float expHeroDamage = weaponDamage * (1f + (8f/100f)); // 8 == intelligence, the Mage's 'damagingAttribute'
        assert testMage.damage() == expHeroDamage;
    }

    @Test
    public void checkDamage_weaponReplaced_correctDamage() throws Exception { // TODO: handle exception?
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create a first Weapon, with valid parameters
        String weaponNameOne = "Bargain Basement Wand";
        int requiredLevelOne = 1;
        Weapon.WeaponType weaponTypeOne = Weapon.WeaponType.WAND;
        int weaponDamageOne = 2;

        Weapon testWeaponOne = new Weapon(weaponNameOne, requiredLevelOne, weaponTypeOne, weaponDamageOne);
        testMage.equip(testWeaponOne); // Equip Weapon
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Create a NEW Weapon, REPLACING the old one (still valid parameters)
        String weaponNameTwo = "Blue Polycarbonate Staff";
        int requiredLevelTwo = 1;
        Weapon.WeaponType weaponTypeTwo = Weapon.WeaponType.STAFF;
        int weaponDamageTwo = 3;

        Weapon testWeaponTwo = new Weapon(weaponNameTwo, requiredLevelTwo, weaponTypeTwo, weaponDamageTwo);
        testMage.equip(testWeaponTwo); // Equip Weapon

        // ASSERT
        // The level 1 Mage's capacity for damage when armed with a 'Blue Polycarbonate Staff' (damage: 3) should be...
        float expHeroDamage = weaponDamageTwo * (1f + (8f/100f)); // 8 == intelligence, the Mage's 'damagingAttribute'
        assert testMage.damage() == expHeroDamage;
    }

    @Test
    public void checkDamage_weaponAndArmorEquipped_correctDamage() throws Exception { // TODO: handle exception?
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create Weapon, with valid parameters
        String weaponName = "Bargain Basement Wand";
        int requiredLevelWeapon = 1;
        Weapon.WeaponType weaponType = Weapon.WeaponType.WAND;
        int weaponDamage = 2;

        Weapon testWeapon = new Weapon(weaponName, requiredLevelWeapon, weaponType, weaponDamage);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Equip weapon
        testMage.equip(testWeapon);

        // Create Armor, with valid parameters
        String armorName = "Colorful Second-Hand Cape";
        int requiredLevelArmor = 1;
        Hero.Slot slot = Hero.Slot.BODY;
        Armor.ArmorType armorType = Armor.ArmorType.CLOTH;
        int armorStrength = 0, armorDexterity = 1, armorIntelligence = 2; // Set up armorAttributes
        HeroAttribute armorAttribute = new HeroAttribute(armorStrength, armorDexterity, armorIntelligence);

        Armor testArmor = new Armor(armorName, requiredLevelArmor, slot, armorType, armorAttribute);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Equip armor
        testMage.equip(testArmor);

        // ASSERT
        // Check for correct damage level when equipped with both a weapon and a piece of armor.
        float damagingAttribute = testMage.getDamagingAttribute();
        float expHeroDamage = weaponDamage * (1f + (damagingAttribute/100f)); // 8 == intelligence, the Mage's 'damagingAttribute'
        assert testMage.damage() == expHeroDamage;
    }
}
