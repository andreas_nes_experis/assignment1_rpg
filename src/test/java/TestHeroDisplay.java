import org.example.Armor;
import org.example.Hero;
import org.example.HeroAttribute;
import org.example.Mage;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.PipedReader;
import java.io.PrintStream;

// TEST SET 7: tests the display() method in Hero.
public class TestHeroDisplay {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }
    //@Rule
    //public final SystemOutRule sysOutRule = new SystemOutRule().enableLog();
    @Test
    public void displayHeroInfo_defaultMage_correctOutput() {
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);
        //ByteArrayOutputStream actuallyDisplayed = new ByteArrayOutputStream();
        //System.setOut(new PrintStream((actuallyDisplayed)));


        // ASSERT
        // Check that the Hero's display method prints out what we expect it to.
        String expectedDisplay = "\nHERO STATS:\n" +
                "NAME: " + testMage.getName() +
                "\nCLASS: " + testMage.getClass() +
                "\nLEVEL: " + testMage.getLevel() +
                "\nDAMAGE LEVEL: " + testMage.damage() +
                "\nATTRIBUTES:" +
                "\n* STRENGTH: " + testMage.totalAttributes().getStrength() +
                "\n* DEXTERITY: " + testMage.totalAttributes().getDexterity() +
                "\n* INTELLIGENCE: " + testMage.totalAttributes().getIntelligence() + "\n";

        testMage.display();
        //System.out.println(di);
        Assert.assertEquals(expectedDisplay, outputStreamCaptor.toString());
    }
}
