
import org.example.*;
import org.junit.Test;

// TEST SET 3: Create a Weapon and a piece of Armor, make sure they have the right properties.
public class TestItemCreation {
    @Test
    public void createWeapon_validParams_correctProperties() {
        // ARRANGE & ACT
        String name = "Common Axe";
        int requiredLevel = 1;
        Weapon.WeaponType weaponType = Weapon.WeaponType.AXE;
        int weaponDamage = 2;

        Weapon testWeapon = new Weapon(name, requiredLevel, weaponType, weaponDamage);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // ASSERT
        assert testWeapon.getName().equals(name);
        assert testWeapon.getRequiredLevel() == requiredLevel;
        assert testWeapon.getSlot() == Hero.Slot.WEAPON;
        assert testWeapon.getWeaponType() == weaponType;
        assert testWeapon.getWeaponDamage() == weaponDamage;
    }

    @Test
    public void createArmor_validParams_correctProperties() throws Exception {
        // ARRANGE & ACT
        String name = "Common Plate Chest";
        int requiredLevel = 1;
        Hero.Slot slot = Hero.Slot.BODY;
        Armor.ArmorType armorType = Armor.ArmorType.PLATE;
        int armorStrength = 1, armorDexterity = 0, armorIntelligence = 0; // Set up armorAttributes
        HeroAttribute armorAttribute = new HeroAttribute(armorStrength, armorDexterity, armorIntelligence);

        Armor testArmor = new Armor(name, requiredLevel, slot, armorType, armorAttribute);
        // TODO: LSP - bytte ut 'Armor' med 'Item'? I saa fall maa jeg lage en felles getType metode.
        // TODO: hvordan haandterer jeg exception her? Burde selve testen throwe det?

        // ASSERT
        assert testArmor.getName().equals(name);
        assert testArmor.getRequiredLevel() == requiredLevel;
        assert testArmor.getSlot() == slot;
        assert testArmor.getArmorType() == armorType;
        assert testArmor.getArmorAttribute() == armorAttribute;
    }
}
