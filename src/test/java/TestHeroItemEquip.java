import org.example.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

// TEST SET 4: Equip Weapons and Armor, throw the appropriate exceptions if invalid level requirement or type.
public class TestHeroItemEquip {

    // TEST WEAPONS: 3 tests
    @Test
    public void equipWeapon_validParamsIn_noProblems() throws Hero.InvalidWeaponException { // TODO: handle exception?
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create Weapon, with valid parameters
        String weaponName = "Bargain Basement Wand";
        int requiredLevel = 1;
        Weapon.WeaponType weaponType = Weapon.WeaponType.WAND;
        int weaponDamage = 2;

        Weapon testWeapon = new Weapon(weaponName, requiredLevel, weaponType, weaponDamage);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Equip weapon
        testMage.equip(testWeapon);

        // ASSERT
        // The level 1 Mage's capacity for damage when armed with a 'Bargain Basement Wand' (damage: 2) should be...
        // If no exception is thrown, and the damage() method returns the expected value, the weapon has been equipped successfully.
        float expHeroDamage = weaponDamage * (1f + (8f/100f)); // Yes, I'm using a magic variable here, for testing purposes (8 == intelligence)
        assert testMage.damage() == expHeroDamage;
    }

    @Rule // This allows us to test for exceptions going forward
    public final ExpectedException exception = ExpectedException.none();
    @Test
    public void equipWeapon_invalidType_throwsException() throws Hero.InvalidWeaponException {
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create Weapon, with valid parameters (the weapon is valid, but can't be used by this Hero)
        String weaponName = "Common Axe";
        int requiredLevel = 1;
        Weapon.WeaponType weaponType = Weapon.WeaponType.AXE;
        int weaponDamage = 2;

        Weapon testWeapon = new Weapon(weaponName, requiredLevel, weaponType, weaponDamage);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // ASSERT
        // Try to equip the weapon, this should throw an exception because 'Mage' cannot equip 'Axe'.
        exception.expect(Hero.InvalidWeaponException.class);
        testMage.equip(testWeapon);
    }

    @Test
    public void equipWeapon_reqLevelTooHigh_throwsException() throws Hero.InvalidWeaponException {
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create Weapon, with valid parameters (the weapon is valid, but can't be used by this Hero)
        String weaponName = "Enterprise-grade Wand";
        int requiredLevel = 500;
        Weapon.WeaponType weaponType = Weapon.WeaponType.WAND;
        int weaponDamage = 9000;

        Weapon testWeapon = new Weapon(weaponName, requiredLevel, weaponType, weaponDamage);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // ASSERT
        // Try to equip the weapon, this should throw an exception because 'Mage' cannot equip 'Axe'.
        exception.expect(Hero.InvalidWeaponException.class);
        testMage.equip(testWeapon);
    }

    // TEST ARMOR: 3 TESTS
    @Test
    public void equipArmor_validParamsIn_noProblems() throws Exception { // TODO: handle exception?
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create Armor, with valid parameters
        String armorName = "Colorful Second-Hand Cape";
        int requiredLevel = 1;
        Hero.Slot slot = Hero.Slot.BODY;
        Armor.ArmorType armorType = Armor.ArmorType.CLOTH;
        int armorStrength = 0, armorDexterity = 1, armorIntelligence = 2; // Set up armorAttributes
        HeroAttribute armorAttribute = new HeroAttribute(armorStrength, armorDexterity, armorIntelligence);

        Armor testArmor = new Armor(armorName, requiredLevel, slot, armorType, armorAttribute);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // Equip armor
        testMage.equip(testArmor);

        // ASSERT
        // Check that the Hero's totalAttributes are at the value expected when wearing the cape
        // If no exception is thrown, and the totalAttributes() method returns the expected values, the armor has been equipped successfully.
        HeroAttribute totalAttributes = testMage.totalAttributes();
        int expStrength = 1, expDexterity = 1 + 1, expIntelligence = 8 + 2; // Ooooh MAGIC variables ('cause it's a MAGE, get it? ;) )
        assert totalAttributes.getStrength() == expStrength;
        assert totalAttributes.getDexterity() == expDexterity;
        assert totalAttributes.getIntelligence() == expIntelligence;
    }

    @Test
    public void equipArmor_invalidType_throwsException() throws Exception {
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create Armor, with valid parameters (the armor is valid, but can't be used by this Hero)
        String armorName = "Common Plate Chest";
        int requiredLevel = 1;
        Hero.Slot slot = Hero.Slot.BODY;
        Armor.ArmorType armorType = Armor.ArmorType.PLATE;
        int armorStrength = 1, armorDexterity = 0, armorIntelligence = 0; // Set up armorAttributes
        HeroAttribute armorAttribute = new HeroAttribute(armorStrength, armorDexterity, armorIntelligence);

        Armor testArmor = new Armor(armorName, requiredLevel, slot, armorType, armorAttribute);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // ASSERT
        // Try to equip the weapon, this should throw an exception because 'Mage' cannot equip 'Axe'.
        exception.expect(Hero.InvalidWeaponException.class);
        testMage.equip(testArmor);
    }

    @Test
    public void equipArmor_reqLevelTooHigh_throwsException() throws Exception {
        // ARRANGE & ACT
        // Create Hero
        String heroName = "Gandalf";
        Hero testMage = new Mage(heroName);

        // Create Armor, with valid parameters (the armor is valid, but can't be used by this Hero)
        String armorName = "Overpriced-But-Nice Designer Cape";
        int requiredLevel = 70;
        Hero.Slot slot = Hero.Slot.BODY;
        Armor.ArmorType armorType = Armor.ArmorType.CLOTH;
        int armorStrength = 5, armorDexterity = 11, armorIntelligence = 25; // Set up armorAttributes
        HeroAttribute armorAttribute = new HeroAttribute(armorStrength, armorDexterity, armorIntelligence);

        Armor testArmor = new Armor(armorName, requiredLevel, slot, armorType, armorAttribute);
        // TODO: LSP - bytte ut 'Weapon' med 'Item'? I saa fall maa jeg lage en felles getType metode.

        // ASSERT
        // Try to equip the weapon, this should throw an exception because 'Mage' cannot equip 'Axe'.
        exception.expect(Hero.InvalidWeaponException.class);
        testMage.equip(testArmor);
    }
}