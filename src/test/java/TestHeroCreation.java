import org.example.*;
import org.junit.Test;

// TEST SET 1: Create all classes of Hero, test for correct name, level, and attributes.
public class TestHeroCreation {
    @Test
    public void createMage_validParams_correctAttr() {
        // ARRANGE & ACT
        String name = "Gandalf";

        Hero testMage = new Mage(name);

        // ASSERT
        int expectedLevel = 1;
        int expStrength = 1, expDexterity = 1, expIntelligence = 8;

        assert testMage.getName().equals(name);
        assert testMage.getLevel() == expectedLevel;
        assert (testMage.totalAttributes().getStrength() == expStrength);
        assert (testMage.totalAttributes().getDexterity() == expDexterity);
        assert (testMage.totalAttributes().getIntelligence() == expIntelligence);
    }

    @Test
    public void createRanger_validParams_correctAttr() {
        // ARRANGE & ACT
        String name = "Rango";

        Hero testRanger = new Ranger(name);

        // ASSERT
        int expectedLevel = 1;
        int expStrength = 1, expDexterity = 7, expIntelligence = 1; // TODO: move to 'assert'?

        assert testRanger.getName().equals(name);
        assert testRanger.getLevel() == expectedLevel;
        assert (testRanger.totalAttributes().getStrength() == expStrength);
        assert (testRanger.totalAttributes().getDexterity() == expDexterity);
        assert (testRanger.totalAttributes().getIntelligence() == expIntelligence);
    }

    @Test
    public void createRouge_validParams_correctAttr() {
        // ARRANGE & ACT
        String name = "Rick Deckard";

        Hero testRouge = new Rouge(name);

        // ASSERT
        int expectedLevel = 1;
        int expStrength = 2, expDexterity = 6, expIntelligence = 1;

        assert testRouge.getName().equals(name);
        assert testRouge.getLevel() == expectedLevel;
        assert (testRouge.totalAttributes().getStrength() == expStrength);
        assert (testRouge.totalAttributes().getDexterity() == expDexterity);
        assert (testRouge.totalAttributes().getIntelligence() == expIntelligence);
    }

    @Test
    public void createWarrior_validParams_correctAttr() {
        // ARRANGE & ACT
        String name = "Pr. Mononoke";

        Hero testWarrior = new Warrior(name);

        // ASSERT
        int expectedLevel = 1;
        int expStrength = 5, expDexterity = 2, expIntelligence = 1;

        assert testWarrior.getName().equals(name);
        assert testWarrior.getLevel() == expectedLevel;
        assert (testWarrior.totalAttributes().getStrength() == expStrength);
        assert (testWarrior.totalAttributes().getDexterity() == expDexterity);
        assert (testWarrior.totalAttributes().getIntelligence() == expIntelligence);
    }
}