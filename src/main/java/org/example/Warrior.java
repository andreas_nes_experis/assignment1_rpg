package org.example;

public class Warrior extends Hero {
    // CONSTRUCTOR
    public Warrior(String name) {
        super(name);
        heroClass = "Warrior";

        // Set starting attributes
        final int strength = 5, dexterity = 2, intelligence = 1;
        levelAttributes = new HeroAttribute(strength, dexterity, intelligence);

        // Define amount attributes are increased when levelling up
        final int strengthLevelUp = 3, dexterityLevelUp = 2, intelligenceLevelUp = 1;
        levelUpAttributeAmount = new HeroAttribute(strengthLevelUp, dexterityLevelUp, intelligenceLevelUp);

        // Set valid weapon and armor types
        validWeaponTypes = new Weapon.WeaponType[]{Weapon.WeaponType.AXE, Weapon.WeaponType.HAMMER, Weapon.WeaponType.SWORD};  // TODO: ok syntax?
        validArmorTypes = new Armor.ArmorType[]{Armor.ArmorType.MAIL, Armor.ArmorType.PLATE};
    }

    // METHOD: returns the level for this kind of hero's 'damaging attribute'
    public float getDamagingAttribute() { // TODO: does the int get converted to float automatically?
        return levelAttributes.getStrength();
    }
}