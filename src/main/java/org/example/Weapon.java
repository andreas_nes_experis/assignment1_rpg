package org.example;

public class Weapon extends Item {
    private WeaponType weaponType;
    private int weaponDamage;

    // CONSTRUCTOR
    public Weapon(String name, int requiredLevel, WeaponType weaponType, int weaponDamage) {
        super(name, requiredLevel);
        this.weaponType = weaponType;
        this.weaponDamage = weaponDamage;
        slot = Hero.Slot.WEAPON;
    }

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public int getWeaponDamage() {
        return weaponDamage;
    }

    // Weapon type enumerator
    public enum WeaponType {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

}
