package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

// TODO: Make the Hero class testable to satisfy the Liscov Substitution Principle
public abstract class Hero {
    // VARIABLES
    protected final String name; // TODO (not important): Refactor?
    protected String heroClass;
    protected int level;
    protected HashMap<Slot, Item> equipment;
    protected HeroAttribute levelAttributes;
    protected HeroAttribute levelUpAttributeAmount;  // Indicates amount attributes are to be increased when leveling up
    protected Weapon.WeaponType[] validWeaponTypes;
    protected Armor.ArmorType[] validArmorTypes;

    // CONSTRUCTOR
    public Hero(String name) {
        // Set name given in parameters, and level to 1.
        this.name = name;
        level = 1;

        // Create HashMap containing keys for each of the four slots, with null values.
        equipment = new HashMap<Slot, Item>();
        equipment.put(Slot.WEAPON, null);
        equipment.put(Slot.HEAD, null);
        equipment.put(Slot.BODY, null);
        equipment.put(Slot.LEGS, null);
    }

    // METHOD: levels up the character, which includes adjusting the LevelAttributes.
    public void levelUp() {
        level++;
        levelAttributes = levelAttributes.addAttributes(levelUpAttributeAmount);
    }

    // METHOD: pretty self-explanatory
    public void equip(Armor armor) throws InvalidWeaponException { // TODO: there must be a better way, w/o converting array to hashset?
        HashSet<Armor.ArmorType> validArmorTypesList = new HashSet<Armor.ArmorType>(List.of(validArmorTypes));

        // Make sure this class of hero can equip this kind of armor
        boolean armorIsValidType = false;
        for (Armor.ArmorType validArmorType : validArmorTypesList) {
            if (armor.getArmorType() == validArmorType) {
                armorIsValidType = true;
                break;
            }
        }
        if (!armorIsValidType) {
                throw new InvalidWeaponException("EXCEPTION: this CLASS of HERO cannot equip this type of ARMOR!");
        }

        // Make sure the hero's level is high enough to equip this piece of armor
        if (armor.getRequiredLevel() > level) {
            throw new InvalidWeaponException("EXCEPTION: the HERO'S LEVEL isn't high enough to equip this piece of ARMOR!");
        }
        else {
            equipment.put(armor.getSlot(), armor);
        }
    }

    // METHOD: same as above, but for weapons
    public void equip(Weapon weapon) throws InvalidWeaponException {
        HashSet<Weapon.WeaponType> validWeaponTypesList = new HashSet<Weapon.WeaponType>(List.of(validWeaponTypes));

        // Make sure this class of hero can equip this kind of weapon
        boolean weaponIsValidType = false;
        for (Weapon.WeaponType validWeaponType : validWeaponTypesList) {
            if (weapon.getWeaponType() == validWeaponType) {
                weaponIsValidType = true;
                break;
            }
        }
        if (!weaponIsValidType) {
            throw new InvalidWeaponException("EXCEPTION: this CLASS of HERO cannot equip this type of WEAPON!");
        }

        // Make sure the hero's level is high enough to equip this weapon
        if (weapon.getRequiredLevel() > level) {
            throw new InvalidWeaponException("EXCEPTION: the HERO'S LEVEL isn't high enough to equip this WEAPON!");
        }
        else {
            equipment.put(Slot.WEAPON, weapon);
        }
    }

    // METHOD: calculates and returns the amount of damage that the hero is currently capable of doing
    public float damage() { // TODO: return type?
        // Find out what the weapon's damage level is
        Weapon weapon = (Weapon) equipment.get(Slot.WEAPON); // TODO: should I cast like this or make a common interface for 'Item' that works for both types?
        int weaponDamage;

        // If no weapon is equipped, weaponDamage is set to 1.
        if (equipment.get(Slot.WEAPON) == null) {
            weaponDamage = 1;
        } else {
            weaponDamage = weapon.getWeaponDamage();
        }
        // Find out what the hero's damaging attribute value is
        float damagingAttribute = getDamagingAttribute();

        // Calculate the hero's damage level
        return weaponDamage * (1 + damagingAttribute/100);
    }

    // METHOD: calculates and returns the hero's total attributes
    public HeroAttribute totalAttributes() { // TODO: return type? AND CLEAN UP THIS MESS! (maybe use a lambda function?)
        HeroAttribute totalAttributes = levelAttributes;
        //equipment.entrySet().stream().filter(item -> item instanceof Armor).forEach(item -> totalAttributes.addAttributes(((Armor) item).getArmorAttribute())); //TODO: whatamess, does it work???
        // Probably not because it doesn't save the returning values from the addAttributes method (it returns a new instance, it doesn't change it's own internal values.
        HashSet<Armor> piecesOfArmor = new HashSet<Armor>();  // TODO: <>? Null?
        piecesOfArmor.add((Armor) equipment.get(Slot.HEAD));
        piecesOfArmor.add((Armor) equipment.get(Slot.BODY));
        piecesOfArmor.add((Armor) equipment.get(Slot.LEGS));
        // if (equipment.get(Slot.HEAD) != null) { TODO: necessary?
        for (Armor pieceOfArmor : piecesOfArmor) {
            if (pieceOfArmor != null) {
                totalAttributes = totalAttributes.addAttributes(pieceOfArmor.getArmorAttribute());
            }
        }
        return totalAttributes;
    }

    // METHOD: displays information about the hero
    public void display() {
        StringBuilder heroDisplay = new StringBuilder("\nHERO STATS:\n"); // Not sure StringBuilder is better than String here.
        heroDisplay.append("NAME: ").append(name);
        heroDisplay.append("\nCLASS: ").append(heroClass);
        heroDisplay.append("\nLEVEL: ").append(level);
        heroDisplay.append("\nDAMAGE LEVEL: ").append(damage());
        heroDisplay.append("\nATTRIBUTES:");
        heroDisplay.append("\n* STRENGTH: ").append(levelAttributes.getStrength());
        heroDisplay.append("\n* DEXTERITY: ").append(levelAttributes.getDexterity());
        heroDisplay.append("\n* INTELLIGENCE: ").append(levelAttributes.getIntelligence()).append("\n");
        System.out.print(heroDisplay);
    }

    // METHOD
    public String getName() {
        return name;
    }

    // METHOD
    public int getLevel() {
        return level;
    }

    // ABSTRACT METHOD: returns the level (as a float) of the Hero's 'damaging attribute'
    abstract public float getDamagingAttribute();
    public static class InvalidWeaponException extends Exception { // TODO: protected?
        public InvalidWeaponException(String errorMessage) {
            super(errorMessage);
        }
    }

    //ENUMERATOR: the four slots
    public enum Slot { // TODO: assignment of slot: in 'item' or here?
        WEAPON,
        HEAD,
        BODY,
        LEGS
    }
}
