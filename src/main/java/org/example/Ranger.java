package org.example;

public class Ranger extends Hero {
    // CONSTRUCTOR
    public Ranger(String name) {
        super(name);
        heroClass = "Ranger";

        // Set starting attributes
        final int strength = 1, dexterity = 7, intelligence = 1;
        levelAttributes = new HeroAttribute(strength, dexterity, intelligence);

        // Define amount attributes are increased when levelling up
        final int strengthLevelUp = 1, dexterityLevelUp = 5, intelligenceLevelUp = 1;
        levelUpAttributeAmount = new HeroAttribute(strengthLevelUp, dexterityLevelUp, intelligenceLevelUp);

        // Set valid weapon and armor types
        validWeaponTypes = new Weapon.WeaponType[]{Weapon.WeaponType.BOW};  // TODO: ok syntax?
        validArmorTypes = new Armor.ArmorType[]{Armor.ArmorType.LEATHER, Armor.ArmorType.MAIL};
    }

    // METHOD: returns the level for this kind of hero's 'damaging attribute'
    public float getDamagingAttribute() {
        return levelAttributes.getDexterity();
    }
}