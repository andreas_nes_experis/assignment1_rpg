package org.example;

public class Mage extends Hero {
    // CONSTRUCTOR
    public Mage(String name) {
        super(name);
        heroClass = "Mage";

        // Set starting attributes
        final int strength = 1, dexterity = 1, intelligence = 8;
        levelAttributes = new HeroAttribute(strength, dexterity, intelligence);

        // Define amount attributes are increased when levelling up
        final int strengthLevelUp = 1, dexterityLevelUp = 1, intelligenceLevelUp = 5;
        levelUpAttributeAmount = new HeroAttribute(strengthLevelUp, dexterityLevelUp, intelligenceLevelUp);

        // Set valid weapon and armor types
        validWeaponTypes = new Weapon.WeaponType[]{Weapon.WeaponType.STAFF, Weapon.WeaponType.WAND};  // TODO: ok syntax?
        validArmorTypes = new Armor.ArmorType[]{Armor.ArmorType.CLOTH};
    }

    // METHOD: returns the level for this kind of hero's 'damaging attribute'
    public float getDamagingAttribute() {
        return levelAttributes.getIntelligence();
    }
}