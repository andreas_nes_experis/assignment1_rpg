package org.example;

public class Armor extends Item {
    private ArmorType armorType;
    private HeroAttribute armorAttribute;

    // CONSTRUCTOR
    public Armor(String name, int requiredLevel, Hero.Slot slot, ArmorType armorType, HeroAttribute armorAttribute) throws Exception {
        super(name, requiredLevel);

        // Check that slot isn't weapon
        if (slot == Hero.Slot.WEAPON) {
            throw new Hero.InvalidWeaponException("EXCEPTION: ARMOR cannot belong in the WEAPON slot!");  // TODO: can the constructor throw exceptions?
        }
        this.slot = slot;
        this.armorType = armorType;
        this.armorAttribute = armorAttribute;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }

    // Armor type enumerator
    public enum ArmorType {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
}
