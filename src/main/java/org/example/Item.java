package org.example;

public abstract class Item {
    protected String name; // TODO: can this be a string or does it need to be a 'Name' class?
    protected int requiredLevel;
    protected Hero.Slot slot;

    // CONSTRUCTOR
    public Item(String name, int requiredLevel) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
    }

    public String getName() {
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public Hero.Slot getSlot() {
        return slot;
    }
}
