package org.example;

public class HeroAttribute {  // TODO: should this class be public and separate from Hero?
    private final int strength; // TODO: should this be final?
    private final int dexterity;
    private final int intelligence;

    public HeroAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    // METHOD: adds two instances together
    public HeroAttribute addAttributes(HeroAttribute toBeAdded) { // TODO: is this how I want to implement this?
        int newStr = strength + toBeAdded.getStrength();
        int newDex = dexterity + toBeAdded.getDexterity();
        int newInt = intelligence + toBeAdded.getIntelligence();
        return new HeroAttribute(newStr, newDex, newInt);
    }
}
