package org.example;

public class Rouge extends Hero {
    // CONSTRUCTOR
    public Rouge(String name) {
        super(name);
        heroClass = "Rouge";

        // Set starting attributes
        final int strength = 2, dexterity = 6, intelligence = 1;
        levelAttributes = new HeroAttribute(strength, dexterity, intelligence);

        // Define amount attributes are increased when levelling up
        final int strengthLevelUp = 1, dexterityLevelUp = 4, intelligenceLevelUp = 1;
        levelUpAttributeAmount = new HeroAttribute(strengthLevelUp, dexterityLevelUp, intelligenceLevelUp);

        // Set valid weapon and armor types
        validWeaponTypes = new Weapon.WeaponType[]{Weapon.WeaponType.DAGGER, Weapon.WeaponType.SWORD};  // TODO: ok syntax?
        validArmorTypes = new Armor.ArmorType[]{Armor.ArmorType.LEATHER, Armor.ArmorType.MAIL};
    }

    // METHOD: returns the level for this kind of hero's 'damaging attribute'
    public float getDamagingAttribute() {
        return levelAttributes.getDexterity();
    }
}